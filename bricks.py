def make_bricks(small, big, goal):
  return False if (small+big*5)<goal or small<(goal%5) else True
